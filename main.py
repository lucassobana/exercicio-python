# media = float(input("Digite a média do aluno: "))
# if media >= 6.0:
#     print("Aprovado")
# elif 6.0 > media >= 4.0:
#     print("NP3")
# else:
#     print("Reprovado")

# t1 = t2 = True
# f1 = f2 = False

# if t1 and f2:
#     print("Expressão verdadeira")
# else:
#     print("Expressão falsa")
    
# if t1 or f2:
#     print("Expressão verdadeira")
# else:
#     print("Expressão falsa")
    
# list = 'Leandro, Guilherme, Pedro, João, Maria'

# nome_1 = 'Leandro'
# nome_2 = 'Lucas'

# if nome_1 in list:
#     print(f'{nome_1} e estão na lista')
# else:
#     print(f'{nome_1} e não estão na lista')

# nota_1 = float(input("Digite a primeira nota: "))
# nota_2 = float(input("Digite a segunda nota: "))

# print(f'A média é: {(nota_1 + nota_2) / 2}')

# ------------WHILE----------------
# cont = 1

# while cont <= 3:
#     nota_1 = float(input("Digite a primeira nota: "))
#     nota_2 = float(input("Digite a segunda nota: "))
    
#     print(f'A média é: {(nota_1 + nota_2) / 2}')
    
#     cont += 1

#------------FOR-------------------
# cont = 1
# for cont in range(1,11):
#     print(cont)

#------------LIST-------------------
# list = ['Lucas', 9.5, 9.0, 8.0, True]
# print(list)
# print(list[0])

# for elements in list:
#     print(elements)
    
# list[3] = 10

# for elements in list:
#     print(elements)

# media = (list[1] + list[2] + list[3]) / 3
# print(f'Media: {media}')

# print(len(list))

# list.append(media)
# print(list)

# list.extend([7.0, 8.0, 9.0])
# print(list)

# list.remove(7.0 )
# print(list)

#------------DICTIONARY-------------------

# dictionary = {
#     'chave_1': 1,
#     'chave_2': 2,
# }

# print(dictionary)

# cadastro = {
#     'matricula': 123,
#     'dia_cadastro': 25,
#     'mes_cadastro': 10,
#     'turma': '2E',
# }

# print(cadastro['matricula'])

# print(cadastro['turma'])

# cadastro['turma']='3E'
# print(cadastro)

# cadastro['modalidade'] = 'EAD'
# print(cadastro)

# cadastro.pop('turma')
# print(cadastro)

# print(cadastro.items())

# print(cadastro.keys())

# print(cadastro.values())

# for chaves, valores in cadastro.items():
#     print(chaves, valores)

#------------FUNCTION-------------------

# def media():
#     nota_1 = float(input("Digite a primeira nota: "))
#     nota_2 = float(input("Digite a segunda nota: "))
    
#     print(f'A média é: {(nota_1 + nota_2) / 2}')
    
# while True:
#     print('1. Calcular média')
#     print('2. Sair')
    
#     opcao = int(input('Digite a opção desejada: '))

#     if opcao == 1:
#         media()
#     elif opcao == 2:
#         break
#     else:
#         print('Opção inválida')

list = [
    {
        'aluno': 'Lucas',
        'nota_1': 6.5,
        'nota_2': 7.5
    },
    {
        'aluno': 'Pedro',
        'nota_1': 5.0,
        'nota_2': 7.5
    }
]

print(list)

aux = int(input('Digite quantos aluno possui: '))

for count in range(0,aux):
    aluno = input('Digite o nome do aluno: ')
    nota_1 = float(input('Digite a primeira nota: '))
    nota_2 = float(input('Digite a segunda nota: '))
    
    list.append({
        'aluno': aluno,
        'nota_1': nota_1,
        'nota_2': nota_2
    })
    
    print('Aluno cadastrado com sucesso')
    
print(list)