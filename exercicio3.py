def cadastro(car):
    marca = input("Digite a marca do veículo: ")
    modelo = input("Digite o modelo do veículo: ")
    preco = float(input("Digite o preço do veículo: "))
    veiculo = {"marca": marca, "modelo": modelo, "preco": preco}
    car.append(veiculo)
    
    print(f"Veículo da marca {marca} e modelo {modelo} cadastrado com sucesso!")
    
def listar(car):
    if not car:
        print("Não há veículos cadastrados no estoque.")
    else:
        print("Veículos disponíveis para venda:")
        for idx, veiculo in enumerate(car, start=1):
            print(f"{idx}. {veiculo['marca']} {veiculo['modelo']} - R${veiculo['preco']:.2f}")

    
def compras(car):
    listar(car)
    if car:
        indice = int(input("Digite o número do veículo que deseja comprar: "))
        if 1 <= indice <= len(car):
            veiculo_comprado = car.pop(indice - 1)
            print(f"Você comprou o veículo {veiculo_comprado['marca']} {veiculo_comprado['modelo']}.")
        else:
            print("Índice inválido. Tente novamente.")
    


car = []
while True:
    
    print("\n-----Bem-vindo à Loja de Veículos----")
    print("1. Cadastrar novo veículo")
    print("2. Listar veículos disponíveis")
    print("3. Comprar veículo")
    print("4. Sair")

    opcao = input("Escolha uma opção: ")

    if opcao == "1":
        cadastro(car)

    elif opcao == "2":
        listar(car)

    elif opcao == "3":
        compras(car)
    
    elif opcao == "4":
        print("Saindo...!")
        break
    else:
        print("Opção inválida.")