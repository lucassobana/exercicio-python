print('\n----------QUESTÃO 1----------')
num_1 = int(input("Digite o primeiro numero: "))
num_2 = int(input("Digite o segundo numero: "))

if num_1 > num_2:
    cont  = num_1
    while cont > num_2+1:
        num_2 += 1
        print(num_2, end=" ")
        
elif num_1 < num_2:
    cont = num_2
    while cont > num_1+1:     
        num_1 += 1
        print(num_1, end=" ")
    

print('\n----------QUESTÃO 2----------')
num_3 = int(input("Digite um numero: "))

for i in range(1,11):
    print(f'{num_3} x {i} = {num_3*i}')

