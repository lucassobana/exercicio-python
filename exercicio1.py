print('\n----------QUESTÃO 1----------')
num_1 = int(input("Digite o primeiro numero: "))
num_2 = int(input("Digite o segundo numero: "))


if num_1 > num_2:
    print(f"O numero {num_1} é maior")
elif num_1 < num_2:
    print(f"O numero {num_2} é maior")

print('\n----------QUESTÃO 2----------')
percentual = float(input("Digite o percentual de aumento: "))

if percentual < 0:
    print(f'Houve um decrescimendo de: {percentual}%')
elif percentual == 0:
    print(f'Não mudou o percentual')
else:
    print(f'Houve um crescimendo de: {percentual}%')